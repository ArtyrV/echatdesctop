#-------------------------------------------------
#
# Project created by QtCreator 2018-04-26T17:26:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EChat
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tray.cpp \
    events.cpp \
    pushmessege.cpp

HEADERS  += mainwindow.h \
    liba.h

FORMS    += mainwindow.ui

RESOURCES += \
    icons.qrc
