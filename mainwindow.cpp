#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tray.cpp"
#include "events.cpp"
#include "pushmessege.cpp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    //внедрение css
    this->setStyleSheet(loadStyleSheet());

    // - шапка
    this->setWindowFlags(Qt::Window|Qt::FramelessWindowHint);
    // +toolbar
    toolb();

    // Работа с трей
this -> setTrayIconActions();
    this -> showTrayIcon();


   connect(ui->closeb,SIGNAL(clicked()),this,SLOT(hide()));

   m_sizeGrip = new QSizeGrip( this );


    ui->gridLayout->addWidget( m_sizeGrip, 0, 3, Qt::AlignTop| Qt::AlignRight );
    m_sizeGrip->setVisible(false);

    ui->label->setAttribute(Qt :: WA_NoMousePropagation);


}






void MainWindow:: toolb()
{
    mainToolBar=new QToolBar();
    mainToolBar->setMovable(false);
    mainToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);

    this->addToolBar(Qt::LeftToolBarArea, mainToolBar);

    i=new QAction (this);
    i->setIcon(QIcon(":/icons/i"));

    messeges=new QAction (this);
    messeges->setIcon(QIcon(":/icons/bubble.png"));

    humans=new QAction (this);
    humans->setIcon(QIcon(":/icons/humans"));

    cube=new QAction (this);
    cube->setIcon(QIcon(":/icons/cube"));

   mainToolBar->addAction(i);
   mainToolBar->addAction(messeges);
   mainToolBar->addAction(humans);
   mainToolBar->addAction(cube);

}






QString MainWindow:: loadStyleSheet()
{
    QString styleSheet;
    QString cssName=":/icons/Simple.css";
    QFile qFile(cssName);
        if (qFile.open(QFile::ReadOnly))
        {
            styleSheet = QLatin1String(qFile.readAll());
        }
        else
        {
            qDebug()<<"Error of css";
        }
return styleSheet;
}


MainWindow::~MainWindow()
{
    delete ui;
}
