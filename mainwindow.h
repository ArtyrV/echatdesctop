#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include<liba.h>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString loadStyleSheet();

private slots:
    void changeEvent(QEvent*);
        void trayIconActivated(QSystemTrayIcon::ActivationReason reason);
        void trayActionExecute();
        void setTrayIconActions();
        void showTrayIcon();


private:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
    Ui::MainWindow *ui;
    QAction *messeges;
    QAction *humans;
    QAction *i;
    QAction *cube;
    QMenu *trayIconMenu;
    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *quitAction;
    QSizeGrip   *m_sizeGrip;
    bool b;
    void pushmassege();
    int storeWidth;
    int storeHeight;

    QPoint global_mpos;
    QPoint rs_mpos;

    QPoint mpos;

   void toolb();

    QSystemTrayIcon *trayIcon;



    QToolBar *mainToolBar;
};

#endif // MAINWINDOW_H
